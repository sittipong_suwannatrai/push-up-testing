//
//  ViewController.m
//  Push Up
//
//  Created by Sittipong Suwannatrai on 4/11/2559 BE.
//  Copyright © 2559 Sittipong Suwannatrai. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;

@end

@implementation ViewController {
    NSUInteger pushUpCounter;
}

- (void)setupSensor {
    // Enabled monitoring of the sensor
    [[UIDevice currentDevice] setProximityMonitoringEnabled:YES];
    
    // Set up an observer for proximity changes
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sensorStateChange:)
                                                 name:@"UIDeviceProximityStateDidChangeNotification" object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    pushUpCounter = 0;
    [self setupSensor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateScoreLabel {
    self.scoreLabel.text = [@(pushUpCounter) stringValue];
}

- (void)sensorStateChange:(NSNotificationCenter *)notification
{
    if ([[UIDevice currentDevice] proximityState] == NO) {
        NSLog(@"Device is ~not~ closer to user.");
        pushUpCounter += 1;
        [self updateScoreLabel];
    } else {
        NSLog(@"Device is close to user.");
    }
}

- (IBAction)pushUpButtonTapped:(id)sender {
    pushUpCounter += 1;
    [self updateScoreLabel];
}

@end
