//
//  main.m
//  Push Up
//
//  Created by Sittipong Suwannatrai on 4/11/2559 BE.
//  Copyright © 2559 Sittipong Suwannatrai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
